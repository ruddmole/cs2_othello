#include "player.h"
#include <stdio.h>
#include <vector>

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
Player::Player(Side side) {
    // Will be set to true in test_minimax.cpp.
    testingMinimax = false;
	printf("MY NAME IS MARY!!!!! FIRST MEMBER ADDED!!!!");
	printf("MY NAME IS RYAN!!!!! SECOND MEMBER ADDED!!!!");
	s = side;
	os = Side((s + 1) % 2);
}

/*
 * Destructor for the player.
 */
Player::~Player() {
}

/*
 * Compute the next move given the opponent's last move. Your AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * msLeft represents the time your AI has left for the total game, in
 * milliseconds. doMove() must take no longer than msLeft, or your AI will
 * be disqualified! An msLeft value of -1 indicates no time limit.
 *
 * The move returned must be legal; if there are no valid moves for your side,
 * return NULL.
 */
Move *Player::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's opponents move before calculating your own move
     **/
	if(opponentsMove != NULL)
    {
		b.doMove(opponentsMove, os);
	}
    if(b.hasMoves(s))
    {
		Move *m = max(b, 1);
		if(m != NULL)
		{
			b.doMove(m, s);
		}
		return m;
	}
	else
	{
		return NULL;
	}
}

Move *Player::max(Board b1, int d) {
	if(b1.hasMoves(s))
	{
		int max = -64;
		int temp;
		Move *move;
		for(int x = 0; x < 8; x++)
		{
			for(int y = 0; y < 8; y++)
			{
				Board *b2 = b1.copy();
				Move *m = new Move(x, y);
				if(b2->checkMove(m, s))
				{
					b2->doMove(m, s);
					temp = min(*b2, d) + moveScore(m);
					if(temp > max)
					{
						move = m;
						max = temp;
					}
					else
					{
						delete m;
					}
				}
				else
				{
					delete m;
				}
			}
		}
		Board *b2 = b1.copy();
		temp = min(*b2, d);
		if(temp > max)
		{
			return NULL;
		}
		return move;
	}
	else
	{
		return NULL;
	}
}

int Player::min(Board b1, int d){
	if(b1.hasMoves(os))
	{
		int min = 64;
		int temp;
		for(int x = 0; x < 8; x++)
		{
			for(int y = 0; y < 8; y++)
			{
				Move *m = new Move(x, y);
				if(b1.checkMove(m, os))
				{
					Board *b2 = b1.copy();
					b2->doMove(m, os);
					temp = minmax(*b2, d - 1) - moveScore(m);
					if(temp < min)
					{
						min = temp;
					}
					else
					{
						delete m;
					}
				}
				else
				{
					delete m;
				}
			}
		}
		Board *b2 = b1.copy();
		temp = minmax(*b2, d);
		if(temp < min)
		{
			min = temp;
		}
		return min;
	}
	else
	{
		return minmax(b1, d - 1);
	}
}

int Player::minmax(Board b1, int d){
	int score;
	if(d <= 0)
	{
		if(s == WHITE)
		{
			score = b1.countWhite() - b1.countBlack();
		}
		else
		{
			score = b1.countBlack() - b1.countWhite();
		}
	}
	else
	{
		if(b1.hasMoves(s))
		{
			int min = 64;
			int temp;
			for(int x = 0; x < 8; x++)
			{
				for(int y = 0; y < 8; y++)
				{
					Move *m = new Move(x, y);
					if(b1.checkMove(m, s))
					{
						Board *b2 = b1.copy();
						b2->doMove(m, s);
						for(int x1 = 0; x1 < 8; x1++)
						{
							for(int y1 = 0; y1 < 8; y1++)
							{
								Move *m2 = new Move(x1, y1);
								if(b2->checkMove(m2, os))
								{
									Board *b3 = b2->copy();
									b3->doMove(m2, os);
									temp = minmax(*b3, d - 1);
									if(temp < min)
									{
										min = temp + moveScore(m) - moveScore(m2);
									}
									else
									{
										delete m2;
									}
								}
								else
								{
									delete m2;
								}
							}
						}
					}
					else
					{
						delete m;
					}
				}
			}		
			score = min;
		}
	}
	return score; 
}
		



int Player::moveScore(Move *m) {
	if((m->getX() == 0 or m ->getX() == 7) and (m->getY() == 0 or m-> getY() == 7))
	{
		return 3;
	}
	else if((m->getX() == 1 or m ->getX() == 6 or m->getX() == 0 or m ->getX() == 7) and 
		(m->getY() == 1 or m-> getY() == 6 or m->getY() == 0 or m-> getY() == 7))
	{
		return 3;
	}
	else if(m->getX() == 0 or m ->getX() == 7 or m->getY() == 0 or m-> getY() == 7)
	{
		return 2;
	}
	else
	{
		return 0;
	}
}
