#ifndef __PLAYER_H__
#define __PLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class Player {

public:
    Player(Side side);
    ~Player();
    
    Move *doMove(Move *opponentsMove, int msLeft);
    int minmax(Board b1, int d);
    Move *max(Board b1, int d);
    int min(Board b1, int d);
    int moveScore(Move *m);

    // Flag to tell if the player is running within the test_minimax context
    bool testingMinimax;
    Board b;
    Side s;
    Side os;
};

#endif
