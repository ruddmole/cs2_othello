Ryan got the AI working. Ryan and Mary both worked on the heuristic function for beating 
the SimplePlayer. 

We tried to get the minimax working. The implementation basically allows us to look 
several moves in advance - that would allow us to make more strategic moves that would 
maximize our scores in the end, even if it means that we have to make sacrifices with 
our current move.